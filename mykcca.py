import numpy as np
from scipy.linalg import eig
from mycentre import centre_in_FS
from mysort import eigSort
def KCCA(X_a, X_b, k, d, gamma):
    ell, _ = X_a.shape
    K_a, K_b = np.zeros((ell,ell)), np.zeros((ell,ell))
    for i in range(ell):        #compute kernel matrices
        for j in range(ell):
            K_a[i,j] = k(X_a[i,:], X_a[j,:])
            K_b[i,j] = k(X_b[i,:], X_b[j,:])
    K_a, K_b = centre_in_FS(K_a), centre_in_FS(K_b) # centre data in FS
    A = np.block([                          # matrices for eigenproblem
        [np.zeros((ell,ell)),  K_a @ K_b],
        [K_b @ K_a , np.zeros((ell,ell))]
    ])
    B = np.block([
        [K_a @ K_a + gamma*np.identity(ell), np.zeros((ell, ell))],
        [np.zeros((ell,ell)), K_b @ K_b +  gamma*np.identity(ell)]
    ])
    lam, U = eig(A,B)                        #solve eigenproblem
    lam, U = eigSort(lam, U)
    lam_d = lam[:d]                          # correlations
    U_ad, U_bd = U[:ell,:d], U[ell:,:d]      # choose eigenvectors
    return K_a @ U_ad, K_b @ U_bd, lam_d     # project data
