import numpy as np
import matplotlib.pyplot as plt
from mycca import CCA

ell = 10
X_a = np.random.normal(0,1,(ell,2))
X_b = np.zeros((ell,2))
X_b[:,0] = np.random.normal(0,1,ell)
for i in range(ell):
    X_b[i,1] = X_a[i,0] + X_a[i,1] - X_b[i,0]

X_a_tf, X_b_tf, U_ak, U_bk, correlation = CCA(X_a,X_b,1)
correlation = np.real(correlation[0])

print(U_ak, U_bk, sep = '\n')

f, (ax1, ax2, ax3) = plt.subplots(1,3)
ax1.set_title('A representation')
ax2.set_title('B representation')
ax3.set_title(f'Transformed data with correlation {correlation}')

ax1.set_xlim(-4,4), ax1.set_ylim(-4,4)
ax1.plot((10*U_ak[0],-10*U_ak[0]),(10*U_ak[1],-10*U_ak[1]),'ko-')
ax1.plot(X_a[:,0],X_a[:,1],'bo')
ax1.set_xlabel('x1')
ax1.set_ylabel('x2')
ax1.set_aspect(1.0)

ax2.set_xlim(-4,4), ax2.set_ylim(-4,4)
ax2.plot((10*U_bk[0],-10*U_bk[0]),(10*U_bk[1],-10*U_bk[1]),'ko-')
ax2.plot(X_b[:,0],X_b[:,1],'ro')
ax2.set_xlabel('y1')
ax2.set_ylabel('y2 = x1 + x2 - y1')
ax2.set_aspect(1.0)

ax3.plot(X_a_tf[:,0], X_b_tf[:,0],'bo')
ax3.set_aspect(1.0)

for i in range(ell):
    ax1.annotate(str(i+1),(X_a[i,0]+0.05,X_a[i,1]+0.05))
    ax2.annotate(str(i+1),(X_b[i,0]+0.05,X_b[i,1]+0.05))
    ax3.annotate(str(i+1),(X_a_tf[i]+0.02,X_b_tf[i]))


plt.show()