import numpy as np
import matplotlib.pyplot as plt
from mykpca import KPCA
from sep_by_label import sep_by_label
from sklearn.datasets import make_moons
from scipy import exp

X, y = make_moons(100)
X1, X2 = sep_by_label(X,y)

def sqeuclidean(x,z):
    return (x - z).T @ (x - z)

def k(x,z):
  return exp(-15*sqeuclidean(x,z))

X_tf = KPCA(X,k,1)
X1_tf, X2_tf = sep_by_label(X_tf, y)


plt.subplot(121)
plt.scatter(*(X1.T),color='blue',alpha=0.5)
plt.plot(*(X2.T),'rx')

plt.subplot(122)
plt.scatter(X1_tf,np.zeros(len(X1_tf)),color='blue',alpha=0.5)
plt.plot(X2_tf,np.zeros(len(X2_tf)),'rx')

plt.show()