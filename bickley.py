import scipy.io
import matplotlib.pyplot as plt
from time import time

import numpy as _np
from scipy.spatial import distance
import numpy as _np
import scipy as _sp
# import scipy.sparse.linalg.eigs as sp_eigs




mat = scipy.io.loadmat('bickley.mat')

X = mat['X']
Y = mat['Y']

def sortEig(A, evs=5, which='LM'):
    '''
    Computes eigenvalues and eigenvectors of A and sorts them in decreasing lexicographic order.
    :param evs: number of eigenvalues/eigenvectors
    :return:    sorted eigenvalues and eigenvectors
    '''
    n = A.shape[0]
    if evs < n:
        d, V = _sp.linalg.eig(A)
    else:
        d, V = _sp.linalg.eig(A)
    ind = d.argsort()[::-1] # [::-1] reverses the list of indices
    return (d[ind], V[:, ind])

class gaussianKernel(object):
    '''Gaussian kernel with bandwidth sigma.'''
    def __init__(self, sigma):
        self.sigma = sigma
    def __call__(self, x, y):
        return _np.exp(-_np.linalg.norm(x-y)**2/(2*self.sigma**2))
    def __repr__(self):
        return 'Gaussian kernel with bandwidth sigma = %f.' % self.sigma

def gramian(X, k):
    '''Compute Gram matrix for training data X with kernel k.'''
    name = k.__class__.__name__
    if name == 'gaussianKernel':
        return _np.exp(-distance.squareform(distance.pdist(X.transpose(), 'sqeuclidean'))/(2*k.sigma**2))
    elif name == 'laplacianKernel':
        return _np.exp(-distance.squareform(distance.pdist(X.transpose(), 'euclidean'))/k.sigma)
    elif name == 'polynomialKernel':
        return (k.c + X.transpose()@X)**k.p
    elif name == 'stringKernel':
        n = len(X)
        # compute weights for normalization
        d = _np.zeros(n)
        for i in range(n):
            d[i] = k.evaluate(X[i], X[i])
        # compute Gram matrix
        G = _np.ones([n, n]) # diagonal automatically set to 1
        for i in range(n):
            for j in range(i):
                G[i, j] = k.evaluate(X[i], X[j]) / _np.sqrt(d[i]*d[j])
                G[j, i] = G[i, j]
        return G
    else:
        #print('User-defined kernel.')
        if isinstance(X, list): # e.g., for strings
            n = len(X)
            G = _np.zeros([n, n])
            for i in range(n):
                for j in range(i+1):
                    G[i, j] = k(X[i], X[j])
                    G[j, i] = G[i, j]
        else:
            n = X.shape[1]
            G = _np.zeros([n, n])
            for i in range(n):
                for j in range(i+1):
                    G[i, j] = k(X[:, i], X[:, j])
                    G[j, i] = G[i, j]
        return G

def kcca(X, Y, k, evs=5, epsilon=1e-6):
    '''
    Kernel CCA.
    
    :param X:    data matrix, each column represents a data point
    :param Y:    lime-lagged data, each column y_i is x_i mapped forward by the dynamical system
    :param k:    kernel
    :param evs:  number of eigenvalues/eigenvectors
    :epsilon:    regularization parameter
    :return:     nonlinear transformation of the data X
    '''
    G_0 = gramian(X, k)
    G_1 = gramian(Y, k)
    
    # center Gram matrices
    n = X.shape[1]
    I = _sp.eye(n)
    N = I - 1/n*_sp.ones((n, n))
    G_0 = N @ G_0 @ N
    G_1 = N @ G_1 @ N
    
    print('now solving eigproblem')
    A = _sp.linalg.solve(G_0 + epsilon*I, G_0, assume_a='sym') \
      @ _sp.linalg.solve(G_1 + epsilon*I, G_1, assume_a='sym')
    print('solved')

    d, V = sortEig(A, evs)
    return (d, V)


# data = _sp.io.loadmat('bickley.mat', squeeze_me=True)
# for s in data.keys():
#     if s[:2] == '__' and s[-2:] == '__': continue
#     exec('%s = data["%s"]' % (s, s))



sigma = 1
k = gaussianKernel(sigma)

evs = 9 # number of eigenfunctions to be computed
d, V = kcca(X, Y, k, evs, epsilon=1e-3)

for i in range(evs):
    fig = plt.figure(figsize=(3, 2))
    plt.scatter(X[0, :], X[1, :], c=V[:, i])
    fig.savefig(f'eigfunction{i}.png',bbox_inches='tight')

# start = time()
# _,_,_ = KCCA(X,Y,k,2,eps)
# end = time()
# print(end-start)
