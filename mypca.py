import numpy as np
from mysort import eigSort
from mycentre import centreData
# X is an (ell times N) np array with ell = data instances, N = data dimensionality
# k is the number of dimensions that will be kept
# Note: Pyhton iterates over the rows of numpy arrays
def PCA(X,k):
    ell, N = X.shape
    X = centreData(X)
    C = 1/ell * X.T @ X             # compute the covariance matrix
    lam, U = np.linalg.eig(C)       # compute the eigenvalue decomp. of C
    lam, U = eigSort(lam, U)        # sort the eigenvectors
    lam, Uk = lam[:k], U[:,:k]      # choose the largest k eigenvectors
    X_tf = np.zeros((ell,k))
    for i in range(ell):            # project the data
        X_tf[i,:] = X[i,:] @ Uk
    return X_tf, Uk ,lam              # return transformed data