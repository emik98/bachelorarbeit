import numpy as np
import matplotlib.pyplot as plt
from mypca import PCA
from sep_by_label import sep_by_label

S = np.random.rand(50)
T = S + np.random.normal(0,0.1,len(S))
X = np.stack((S,T), axis = 1)
labels = [x >= 0.5 for x in X[:,0]]

X1, X2 = sep_by_label(X, labels)

X_tf, eigVec, _ = PCA(X,1)
X1_tf, X2_tf = sep_by_label(X_tf, labels)

plt.subplot(121)
plt.xlim(-0.1,1.1), plt.ylim(-0.2,1.2)
plt.plot(*zip(-10*eigVec,10*eigVec),'ko-') # uncomment to leave out eigenvector
plt.plot(*(X1.T),'bo')
plt.plot(*(X2.T),'rx')

plt.subplot(122)
plt.plot(X1_tf,np.zeros(len(X1_tf)),'bo')
plt.plot(X2_tf,np.zeros(len(X2_tf)),'rx')

plt.show()