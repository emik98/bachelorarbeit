import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from mypca import PCA
from mycentre import centreData


iris = datasets.load_iris()
# print(iris)
X = iris['data']
Y = iris['target']
Y = list(Y)

X_c = centreData(X)
X_c_sq =  np.square(X_c)
variances = 1/len(X_c_sq) * sum(X_c_sq)
total_variance = sum(variances)
# print(total_variance)

setosa_count = Y.count(0)
versicolor_count = Y.count(1) + setosa_count

X_tf, PC, corr = PCA(X, 2)
print(*zip(*PC))

def sep_into_three(X):
    setosa = X[:setosa_count,:]
    versicolor = X[setosa_count:versicolor_count,:]
    virginica = X[versicolor_count:,:]
    return setosa, versicolor, virginica

setosa, versicolor, virginica = sep_into_three(X_tf)
# print(corr)

fig, ax = plt.subplots()

ax.scatter(setosa[:,0],setosa[:,1],c='b', label='setosa', alpha=0.5)
ax.scatter(versicolor[:,0],versicolor[:,1],c='r', label='versicolor', alpha=0.5)
ax.scatter(virginica[:,0],virginica[:,1],c='g', label='viriginica', alpha=0.5)
ax.set_xlabel(f'1st PC capturing {(corr[0]/total_variance*100):.2f}% of total variance')
ax.set_ylabel(f'2nd PC capturing {(corr[1]/total_variance*100):.2f}% of total variance')

ax.quiver(np.zeros(4),np.zeros(4),*zip(*PC),units='xy',scale = 0.5,
          zorder=3, color='blue', width=0.01, headwidth=4., headlength=7.)
ax.axis('equal')

ax.set_title("2-D PCA of Fisher's Iris data set")
ax.legend(title='Species')
ax.annotate('Sepal width',xy=(-0.35,-1.55))
ax.annotate('Sepal length',xy=(0.55,-1.4))
ax.annotate('Petal width',xy=(0.5,0.21))
ax.annotate('Petal length',xy=(1.75,0.37))

plt.show()
