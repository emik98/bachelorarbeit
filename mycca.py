import numpy as np
from scipy.linalg import eig                # generalised eigenproblem solver
from mysort import eigSort
from mycentre import centreData
def CCA(X_a, X_b, k):
    ell, N_a = X_a.shape
    _, N_b = X_b.shape
    X_a = centreData(X_a)
    X_b = centreData(X_b)
    # compute covariance matrices
    C_aa, C_ab = 1 / ell * X_a.T @ X_a, 1/ell * X_a.T @ X_b
    C_ba, C_bb =  C_ab.T, 1 / ell * X_b.T @ X_b
    A = np.block([                          # matrices for eigenproblem
        [np.zeros((N_a,N_a)), C_ab               ],
        [C_ba,                np.zeros((N_b,N_b))]
    ])
    B = np.block([
        [C_aa,                 np.zeros((N_a, N_b))],
        [np.zeros((N_b, N_a)), C_bb                ]
    ])
    lam, U = eig(A,B)                         # solve eigenproblem
    lam, U = eigSort(lam, U)                  # sort eigenvectors
    lam_k = lam[:k]                           
    U_ak = U[:N_a,:k]                         # choose eigenvectors
    U_bk = U[N_a:,:k]
    X_a_tf = np.zeros((ell,k))
    X_b_tf = np.zeros((ell,k))
    for i in range(ell):                     # project data
        X_a_tf[i,:] = X_a[i,:] @ U_ak
        X_b_tf[i,:] = X_b[i,:] @ U_bk
    return X_a_tf, X_b_tf, U_ak, U_bk, lam_k