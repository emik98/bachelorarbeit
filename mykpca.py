import numpy as np
from mycentre import centre_in_FS
from mysort import eigSort
# X is the (ell x N) matrix, storing embedded data instances as rows
# k is the kernel function
# d is the number of features we wish to keep
def KPCA(X, k, d):
    ell, N = X.shape
    K = np.zeros((ell,ell))
    for i in range(ell):            # compute kernel matrix
        for j in range(ell):
            K[i,j] = k(X[i,:],X[j,:])
    K = centre_in_FS(K)             # centre data in FS
    lam, V = np.linalg.eig(K)       # compute eigendecomposition of K
    lam, V = eigSort(lam, V)        # sort eigenvectors
    Ad = np.zeros((d, ell))
    for i in range(d):
        Ad[i,:] = ((lam[i]**(-1/2)) * V[:,i]).T
    return (Ad @ K).T, Ad           # return transformed data