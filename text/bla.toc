\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Linear algorithms: PCA and CCA}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Principal Component Analysis}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Canonical Correlation Analysis}{9}{subsection.2.2}
\contentsline {section}{\numberline {3}Kernels and Kernelised Algorithms: KPCA and KCCA}{14}{section.3}
\contentsline {subsection}{\numberline {3.1}Kernels}{14}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Motivation}{14}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Definition and Characterisations of Kernels}{15}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Kernel Construction and Examples}{22}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}Kernel PCA}{24}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Kernel CCA}{27}{subsection.3.3}
\contentsline {section}{\numberline {4}Applications}{31}{section.4}
\contentsline {subsection}{\numberline {4.1}Data Visualisation}{31}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Data Preprocessing}{32}{subsection.4.2}
\contentsline {section}{\numberline {5}Appendix}{35}{section.5}
\contentsline {subsection}{\numberline {5.1}Auxiliary Python Code}{35}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Link to Repository}{35}{subsection.5.2}
