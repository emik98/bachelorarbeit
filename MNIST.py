import scipy.io
from scipy import exp
import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from mykpca import KPCA
import time

from sklearn.decomposition import KernelPCA


start_import = time.time()
mat = scipy.io.loadmat('MNIST.mat')
end_import = time.time()



tr_img = mat['tr_img']
tr_lbl = mat['tr_lbl']
te_img = mat['te_img']
te_lbl = mat['te_lbl']

ell = 3000
ell_test = 500

X = np.zeros((ell + ell_test,784))
y = np.zeros(ell + ell_test)

# X_test = np.zeros((ell_test,784))
# y_test = np.zeros(ell_test)


for i in range(ell+ell_test):
    X[i,:] = tr_img[:,:,i].flatten()
    y[i] = tr_lbl[:,i]


# def sqeuclidean(x,z):
#     return (x - z).T @ (x - z)

# def k(x,z):
#   return exp(-15*sqeuclidean(x,z))

def k(x,z):
    return (np.dot(x,z) + 1)**2
# sk_kpca = KernelPCA(n_components = 100,kernel='poly',degree=2,gamma=1)

X_test = X[ell:,:]
X = X[:ell,:]

start_kpca = time.time()
X_tf, Ad = KPCA(X,k,300)
end_kpca = time.time()

K_new = np.zeros((ell,ell_test))
for i in range(ell):
    for j in range(ell_test):
        K_new[i,j] = k(X[i,:],X_test[j,:])

X_tf_test = (Ad @ K_new).T


y_test = y[ell:]
y = y[:ell]

print(X.shape)
print(X_test.shape)


# for i in range(ell_test):
#     X_test[i,:] = te_img[:,:,i].flatten()
#     y_test[i] = te_lbl[:,i]

start_total = time.time()

clf = SVC(kernel='poly',degree=2,gamma=1,coef0=1)

start_fit = time.time()
clf.fit(X,y)
end_fit = time.time()

start_prediction = time.time()
y_prediction = clf.predict(X_test)
end_prediction = time.time()

end_total = time.time()


print("error % with polynomial SVM:", sum([y_prediction[i] != y_test[i] for i in range(ell_test)])/ell_test*100,"%")

start_total_kpca = time.time()

clf_kpca = SVC(kernel='linear')

start_fit_kpca = time.time()
clf_kpca.fit(X_tf,y)
end_fit_kpca = time.time()

start_prediction_kpca = time.time()
y_prediction_kpca = clf_kpca.predict(X_tf_test)
end_prediction_kpca = time.time()

print("error % with KPCA + linear SVM:", sum([y_prediction_kpca[i] != y_test[i] for i in range(ell_test)])/ell_test*100,"%")

# print(y_test)

end_total_kpca = time.time()

time_import = end_import-start_import
time_fit = end_fit-start_fit
time_prediction = end_prediction-start_prediction
time_total = end_total - start_total
time_kpca = end_kpca - start_kpca
time_fit_kpca = end_fit_kpca - start_fit_kpca
time_prediction_kpca = end_prediction_kpca - start_prediction_kpca
time_total_kpca = end_total_kpca - start_total_kpca + time_kpca



print("import:",time_import)

print("fit:", time_fit)
print("prediction:", time_prediction)
print("total poly SVM:", time_total)

print("KPCA:", time_kpca)
print("fit with KPCA:", time_fit_kpca)
print("prediction with KPCA:", time_prediction_kpca)
print("total with KPCA + linear SVM:", time_total_kpca)