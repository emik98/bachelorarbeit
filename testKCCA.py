import numpy as np
import matplotlib.pyplot as plt
from math import pi
from mykcca import KCCA
from scipy import exp

ell = 100
vals = np.random.rand(ell)
for i in range(ell): vals[i] = vals[i] * 2 * pi


x = np.cos(vals) + np.random.normal(0,0.05,ell)
y = np.sin(vals) + np.random.normal(0,0.05,ell)

x2 = np.random.rand(ell)
y2 = np.zeros(ell)
for i in range(ell): 
    x2[i] = x2[i] * pi - pi/2
    y2[i] = np.sin(2*x2[i]) + np.random.normal(0,0.05)

def sqeuclidean(x,z):
    return (x - z).T @ (x - z)

def k(x,z):
  return exp(-100*sqeuclidean(x,z))


x_tf, y_tf, corr = KCCA(np.stack((x,y),axis=1),np.stack((x2,y2),axis=1),k,1,0.1)

print(corr)

plt.subplot(121)
plt.scatter(x,y,c='b',alpha=0.5)
plt.scatter(x2,y2,c='r',alpha=0.5)
plt.gca().set_aspect('equal', adjustable='box')
plt.xlim(-1.6,1.6), plt.ylim(-1.6,1.6)


plt.subplot(122)
plt.scatter(x_tf,y_tf,c='k',alpha=0.5)
plt.gca().set_aspect('equal', adjustable='box')


plt.show()