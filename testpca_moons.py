import numpy as np
import matplotlib.pyplot as plt
from mypca import PCA
from sep_by_label import sep_by_label
from sklearn.datasets import make_moons

X, y = make_moons(100)
X1, X2 = sep_by_label(X,y)

X_tf, eigVec, _ = PCA(X,1)
X1_tf, X2_tf = sep_by_label(X_tf, y)


plt.subplot(121)
plt.scatter(*(X1.T),color='blue',alpha=0.3)
plt.plot(*(X2.T),'rx')

plt.subplot(122)
plt.scatter(X1_tf,np.zeros(len(X1_tf)),color='blue',alpha=0.3)
plt.plot(X2_tf,np.zeros(len(X2_tf)),'rx')
plt.show()

