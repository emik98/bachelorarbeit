import numpy as np
def eigSort(eigvals, eigvecs):
    idx = np.argsort(eigvals)[::-1]
    eigvals = eigvals[idx]  
    eigvecs = eigvecs[:,idx]
    return eigvals, eigvecs