import scipy.io
import matplotlib.pyplot as plt
from time import time

import numpy as _np
from scipy.spatial import distance
import numpy as _np
import scipy as _sp
from scipy import exp
from mykcca import KCCA
# import scipy.sparse.linalg.eigs as sp_eigs




mat = scipy.io.loadmat('bickley.mat')

X = mat['X'].T
Y = mat['Y'].T

def sqeuclidean(x,z):
    return (x - z).T @ (x - z)

def k(x,z):
  return exp(-1/2*sqeuclidean(x,z))



evs = 9 # number of eigenfunctions to be computed

V,_,_ = KCCA(X, Y, k, evs, 1e-3)

for i in range(evs):
    fig = plt.figure(figsize=(3, 2))
    plt.scatter(X[:, 0], X[:, 1], c=V[:, i])
    fig.savefig(f'eigfunction{i}.png',bbox_inches='tight')


