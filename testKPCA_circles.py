import numpy as np
import matplotlib.pyplot as plt
from mykpca import KPCA
from sep_by_label import sep_by_label
from sklearn.datasets import make_circles
from scipy import exp

X, y = make_circles(n_samples=400, factor=.3, noise=.05)
X1, X2 = sep_by_label(X,y)

def sqeuclidean(x,z):
    return (x - z).T @ (x - z)

def k(x,z):
  return exp(-15*sqeuclidean(x,z))

X_tf = KPCA(X,k,2)
X1_tf, X2_tf = sep_by_label(X_tf, y)


plt.subplot(121)
plt.scatter(*(X1.T),color='blue',alpha=0.5)
plt.scatter(*(X2.T),color='red',alpha=0.5)

plt.subplot(122)
plt.scatter(*(X1_tf.T),color='blue',alpha=0.5)
plt.scatter(*(X2_tf.T),color='red',alpha=0.5)

plt.show()