import numpy as np
def centreData(X):
    ell, _ = X.shape
    mu = 1/ell * sum(X)
    for i in range(ell):
        X[i,:] = X[i,:] - mu
    return X

def centre_in_FS(K):
    ell, _ = K.shape
    D = sum(K) / ell
    E = sum(D) / ell 
    J = np.ones((ell,1)) @ np.reshape(D, (1,ell))
    return K - J - J.T + E * np.ones((ell, ell))