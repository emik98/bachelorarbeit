import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from mykpca import KPCA
from mycentre import centreData
from scipy import exp, tanh, sin

def k_pol(x,z):
    return (np.dot(x,z)+10)**2

def k_RBF(x,z):
  return exp(-3*np.dot(x-z,x-z))

def k_sig(x,z):
    return tanh(50*np.dot(x,z)-1)

def k_experimental(x,z):
    return exp(-3*np.dot(x-z,x-z))

iris = datasets.load_iris()
# print(iris)
X = iris['data']
Y = iris['target']
Y = list(Y)
print(len(X))

X_c = centreData(X)
X_c_sq =  np.square(X_c)
variances = 1/len(X_c_sq) * sum(X_c_sq)
total_variance = sum(variances)
# print(total_variance)

setosa_count = Y.count(0)
versicolor_count = Y.count(1) + setosa_count
print('setosa:',setosa_count)
print('versicolor',versicolor_count)
print('virginica:',Y.count(2))

X_tf = KPCA(X,np.dot, 2)

X_tf_nl = KPCA(X,k_experimental,2)
# print(*zip(*PC))

def sep_into_three(X):
    setosa = X[:setosa_count,:]
    versicolor = X[setosa_count:versicolor_count,:]
    virginica = X[versicolor_count:,:]
    return setosa, versicolor, virginica

setosa, versicolor, virginica = sep_into_three(X_tf)

setosa_nl, versicolor_nl, virginica_nl = sep_into_three(X_tf_nl)
# print(corr)

fig, (ax1, ax2) = plt.subplots(1,2)

ax1.scatter(setosa[:,0],setosa[:,1],c='b', label='setosa', alpha=0.5)
ax1.scatter(versicolor[:,0],versicolor[:,1],c='r', label='versicolor', alpha=0.5)
ax1.scatter(virginica[:,0],virginica[:,1],c='g', label='viriginica', alpha=0.5)

ax2.scatter(setosa_nl[:,0],setosa_nl[:,1],c='b', label='setosa', alpha=0.5)
ax2.scatter(versicolor_nl[:,0],versicolor_nl[:,1],c='r', label='versicolor', alpha=0.5)
ax2.scatter(virginica_nl[:,0],virginica_nl[:,1],c='g', label='viriginica', alpha=0.5)


plt.show()
