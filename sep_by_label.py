import numpy as np
def sep_by_label(X, labels):
    X1 = []
    X2 = []
    for i in range(len(X)):
        if labels[i]:
            X1.append(X[i,:])
        else:
            X2.append(X[i,:])
    X1 = np.stack(X1, axis=0)
    X2 = np.stack(X2, axis=0)
    return X1, X2